from abc import ABCMeta, abstractmethod
import sys
import numpy as np
import itertools

class Model(metaclass=ABCMeta):
  def __init__(self, labels, features):
    self.labels   = labels
    self.features = features

  @abstractmethod
  def train(self, data):
    pass

  @abstractmethod
  def classify(self, features):
    return None

  def test(self, data):
    count = 0
    for test in data:
      real = test[-1]
      got  = self.classify(test[:-1])
      if real == got:
        count += 1
      else:
        print("Error in test", test, "got", got)
    return count

class Perceptron(Model):
  def __init__(self, labels, features, max_fails, c):
    super().__init__(labels, features)
    self.weights = np.zeros((labels,features), dtype=np.float64)
    self.max_fails = max_fails
    self.c = c

  def train(self, data):
    converged = False
    count = 0
    while not converged:
      converged = True
      count += 1
      fails = 0
      for sample in data:
        f    = sample[:-1]
        real = sample[-1]
        got  = self.classify(f)
        if got != real:
          wy  = self.weights[got]
          wyx = self.weights[real]
          t = np.dot(wy - wyx, f) + 1 / np.dot(2*f, f)
          t = min(t, self.c) #cap the value of t
          self.weights[got]  = wy  - (t*f)
          self.weights[real] = wyx + (t*f)
          fails += 1
      if fails > self.max_fails:
        converged = False
    return count

  def classify(self, features):
    best  = -sys.maxsize
    label = None
    for counter, weight_vec in enumerate(self.weights):
      v = np.dot(weight_vec, features)
      if v > best:
        best  = v 
        label = counter
    return label

Model.register(Perceptron)

class NaiveBayes(Model):
  def __init__(self, labels, features, features_characteristics):
    super().__init__(labels, features)
    self.labels_probabilities   = np.zeros(labels)
    self.features_probabilities = []
    for min_v, max_v, laplace in features_characteristics:
      keys = list(itertools.product(range(labels), range(min_v, max_v + 1)))
      feature = dict.fromkeys(keys, laplace)
      self.features_probabilities.append(feature)
      
  def train(self, data):
    for sample in data:
      label = sample[-1]
      self.labels_probabilities[label] += 1
      for feature_i, feature_v in enumerate(sample[:-1]):
        self.features_probabilities[feature_i][(label, feature_v)] += 1
      for feature_p in self.features_probabilities:
        feature_l = [0]*self.labels
        for k,p in feature_p.items():
          l, _ = k
          feature_l[l] += p
        for l, f in feature_p:
          feature_p[(l,f)] /= feature_l[l]
    self.labels_probabilities /= len(data)
    return 1

  def classify(self, features):
    best = 0
    label = None
    for l in range(self.labels):
      v = self.labels_probabilities[l]
      for feature_i, feature_v in enumerate(features):
        v *= self.features_probabilities[feature_i][(l, feature_v)]
      if v > best:
        best  = v 
        label = l
    return label

Model.register(NaiveBayes)
