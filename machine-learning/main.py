#!/usr/bin/env python3.6

from models import Model, Perceptron, NaiveBayes
import numpy as np
import time
import sys

def main():
  data = np.loadtxt("fisher.csv", np.int, delimiter=",")
  np.random.shuffle(data)
  train = data[:120]
  test  = data[120:]

  print("Avaliable models:\n* nb: NaiveBayes\n* p: Perceptron")
  m = input("choose model: ")
  model = None
  labels   = 3
  features = 4
  if m == "p":
    c = 0.3
    max_fails = 7
    model = Perceptron(labels, features, max_fails, c)
  elif m == "nb":
    features_characteristics = [(0, 30, 5), (0, 80, 5), (10, 50, 5), (35, 90, 5)]
    model = NaiveBayes(labels, features, features_characteristics)
  else:
    sys.exit("unkown model: " + m) 

  t1 = time.time()
  rounds = model.train(train)
  t2 = time.time()
  print("train take", rounds, "rounds and", t2 - t1, "seconds")
  success = model.test(test)
  print("success rate {0:.2f}%".format(success / 30 * 100))

if __name__ == "__main__":
  main()
