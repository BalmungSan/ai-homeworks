#imports
from collections import defaultdict

#d-separation algorithm for bayes nets
class dseparation:
  #constructor, takes a bayes net
  def __init__(self, bayes_net):
    self.bayes_net = bayes_net
    #create an undirected graph from the bayes net
    self.graph = defaultdict(lambda: set())
    for node in bayes_net:
      sons = bayes_net[node]
      self.graph[node].update(sons.copy())
      for s in sons:
        self.graph[s].add(node)

  #check independence between two nodes in the bayes net, giving evidence
  def check_independence(self, n1, n2, evidence):
    #get all paths from two nodes
    def all_paths(start, end, path=[]):
      #if already at end return the path
      path = path + [start]
      if (start == end):
        return [path]

      #if not, get all paths recursively
      paths = []
      for node in self.graph[start]:
        if (node not in path):
          for newpath in all_paths(node, end, path):
            paths.append(newpath)

      return paths

    #check if a path is active
    def check_active_path(path):
      #check if a triple is active
      def check_active_triple(n1, n2, n3):
        #
        def get_children(n):
          children = self.bayes_net[n]
          for son in children.copy():
            children.union(get_children(son))
          return children

        #get the type of the triple
        def get_case(n1,n2, n3):
          if (n2 in self.bayes_net[n1] and n3 in self.bayes_net[n2]
              or n2 in self.bayes_net[n3] and n1 in self.bayes_net[n2]):
            #causal chain
            return "-"
          elif (n1 in self.bayes_net[n2] and n3 in self.bayes_net[n2]):
            #common cause
            return "^"
          elif (n2 in self.bayes_net[n1] and n2 in self.bayes_net[n3]):
            #common effect
            return "v"
          else:
            return None

        #given the type of the triple, use the rules to check if its active or not
        case = get_case(n1, n2, n3)
        if (case == "-"):
          #causal chain
          if (n2 in evidence):
            return False
          else:
            return True
        elif (case == "^"):
          #common cause
          if (n2 in evidence):
            return False
          else:
            return True
        elif (case == "v"):
          #common effect
          if (n2 in evidence):
            return True
          else:
            #t structure
            for son in get_children(n2):
              if (son in evidence):
                return True
            return False
        else:
          return None

      #if any triple is inactive, path is inactive
      for i in range(1, len(path) - 1):
        if (not check_active_triple(path[i - 1], path[i], path[i + 1])):
          return False

      #if all triples where active, path is active
      return True

    #if any path is active, can't guaranteed independence
    for path in all_paths(n1, n2):
      if (check_active_path(path)):
        return False

    #if all paths where inactive, can guarantee independence
    return True
