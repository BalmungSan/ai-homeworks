#!/usr/bin/python3

#importst
from dseparation import dseparation
import sys

#main method
def main():
  bayes_net = {'U': set(['W']),
               'V': set(['W', 'X', 'T']),
               'W': set(['Y']),
               'X': set(['Y']),
               'Y': set(),
               'T': set(['Z']),
               'Z': set()}
  print(bayes_net)
  bayes_net = dseparation(bayes_net)
  n1, n2    = 'V', 'Z'
  evidence  = set(['T'])
  print(bayes_net.check_independence(n1, n2, evidence))

#start the app
main()
