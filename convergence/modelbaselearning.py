#!/usr/bin/python3
#imports
import sys
import numpy as np

#for each state remember the number of times reached and all transitions
#for each transition remember the reward and the number of times it happened
transitions = {'A': (0, {'X': (0,0)}),
               'B': (0, {'C': (0,0)}),
               'C': (0, {'A': (0,0), 'D': (0,0), 'E': (0,0)}),
               'D': (0, {'X': (0,0)}),
               'E': (0, {'C': (0,0)})}

#execute the policy and update the state values
def eval():
    #init
    state = np.random.choice(['B', 'E'])
    run = True
    
    #while a terminal state is not reached, move according the policy and update the transition values
    while run:
        oldstate = state

        if (state == 'A'):
            run = False #terminal state
            state = 'X'
            r = -10
        elif (state == 'B'):
            state = 'C'
            r = -1
        elif (state == 'C'):
            state = np.random.choice(['A', 'D', 'E'], p=[0.1, 0.8, 0.1])
            r = -1
        elif (state == 'D'):
            run = False #terminal state
            state = 'X'
            r = 10
        elif (state == 'E'):
            state = 'C'
            r = -1
        else:
            #???
            print('Error, unknown state: ' + state)
            run = False

        #
        timess, trans = transitions[oldstate]
        reward, timest = trans[state]
        trans[state] = (reward + r, timest + 1)
        transitions[oldstate] = (timess + 1, trans)
    
#Main method
def main():
    #learn 'iterations' times
    iterations = int(sys.argv[1])
    for _ in range(iterations):
        eval() 
    
    #print the converged r and t values
    print('converged r values')
    for state1 in sorted(transitions):
        timess, trans = transitions[state1]
        for state2 in sorted(trans):
            reward, timest = trans[state2]
            if (timest == 0):
                print('(' + state1 + ',' + state2 + ') -> r: 0\tt: 0') 
            else:
                print('(' + state1 + ',' + state2 + ') -> r: ' + str(reward/timest) + '\tt: ' + str(timest/timess))

main()
