#!/usr/bin/python3
#imports
import sys
import numpy as np

#for each state remember the accumulated value and the number of times the state was visited
states = {'A': (0,0), 'B': (0,0), 'C': (0,0), 'D': (0,0), 'E': (0,0)}

#execute the policy and update the state values
def eval():
    #init
    score = []
    state = np.random.choice(['B', 'E'])
    run = True
    
    #while a terminal state is not reached, move according the policy
    while run:
        if (state == 'A'):
            score.append(('A', -10))
            run = False #terminal state
        elif (state == 'B'):
            state = 'C'
            score.append(('B', -1))
        elif (state == 'C'):
            state = np.random.choice(['A', 'D', 'E'], p=[0.1, 0.8, 0.1])
            score.append(('C', -1))
        elif (state == 'D'):
            score.append(('D', 10))
            run = False #terminal state
        elif (state == 'E'):
            state = 'C'
            score.append(('E', -1))
        else:
            #???
            print('Error, unknown state: ' + state)
            run = False
    
    #update the state values 
    reward = 0
    while score:
        state, value = score.pop()
        acc, times = states[state]
        reward += value
        states[state] = (acc + reward, times + 1)

#Main method
def main():
    #learn 'iterations' times
    iterations = int(sys.argv[1])
    for _ in range(iterations):
        eval() 
    
    #print the converged v values
    for state in sorted(states):
        acc, times = states[state]
        if (times == 0):
            print(state + ' -> 0')
        else:
            print(state + ' -> ' + str(acc/times))

main()
