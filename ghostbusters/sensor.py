#imports
import numpy as np
import itertools

#sensor class, feels if a ghost is near or not
class sensor:
  #constructor, initialize the initial state
  def __init__(self):
    #initial state
    self.ghost  = (0,0)
    self.size   = 3
    probability = 1 / (self.size * self.size)
    self.state  = np.full((self.size, self.size), probability, dtype=np.float64)
    self.colors = ['R', 'O', 'Y', 'G']

    #this sensor
    self.sensor ={('R',0): 0.700, ('R',1): 0.300, ('R',2): 0.175, ('R',3): 0.050, ('R',4): 0.030, 
                  ('O',0): 0.200, ('O',1): 0.500, ('O',2): 0.325, ('O',3): 0.150, ('O',4): 0.070,
                  ('Y',0): 0.070, ('Y',1): 0.150, ('Y',2): 0.325, ('Y',3): 0.500, ('Y',4): 0.020,
                  ('G',0): 0.030, ('G',1): 0.050, ('G',2): 0.175, ('G',3): 0.300, ('G',4): 0.070}

  #feel a given position, returns the sensed color and the calculated probability distribution
  def feel(self, position):
    #aux method, calculate the distance between two points
    def distance(p1, p2):
      y1, x1 = p1
      y2, x2 = p2
      return abs(y2 - y1) + abs(x2 -x1)

    #feel the position and get a color
    dist  = distance(self.ghost, position)
    color = np.random.choice(self.colors, p=list(self.sensor[(c,dist)] for c in self.colors))

    #calculate the probability of a position given the sensed color
    for point in itertools.product(range(self.size), range(self.size)):
      dist = distance(point, position)
      aux  = self.sensor[(color, dist)] * self.state[point]
      self.state[point] = aux

    #normalize the state of the sensor
    self.state = self.state / np.sum(self.state)

    #return the sensed color and the calculated probabilities
    return (color)
