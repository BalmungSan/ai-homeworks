#!/usr/bin/python3

#imports
from sensor import sensor
import sys

#main method
def main():
  #initialize sensor
  sen = sensor()
  print(sen.state)
  print("///////////////////////////////////////")

  #read until EOF
  for line in sys.stdin:
    #cast the user input to a tuple (Y, X)
    pos = tuple(int(x) for x in line.split())
    #feel that position
    c = sen.feel(pos)

    #print results
    print("position", pos, "color", c)
    print(sen.state)
    print("///////////////////////////////////////")

#start the app
main()
