#!/usr/bin/python3

#imports
from bayes_nets import BayesNet
from likelihood_weighting import LikelihoodWeighting
from pprint import pprint

#main method
def main():
  nodes = {"C": ["R", "S"],
           "R": ["W"],
           "S": ["W"],
           "W": []
          }

  probabilities = {"C": frozenset({
                          frozenset({"C":True}.items()) : 0.5,
                          frozenset({"C":False}.items()): 0.5
                        }.items()),
                   "R": frozenset({
                          frozenset({"C":True, "R":True}.items()) : 0.8, 
                          frozenset({"C":True, "R":False}.items()): 0.2, 
                          frozenset({"C":False,"R":True}.items()) : 0.2, 
                          frozenset({"C":False,"R":False}.items()): 0.8
                        }.items()),
                   "S": frozenset({
                          frozenset({"C":True, "S":True}.items()) : 0.1,
                          frozenset({"C":True, "S":False}.items()): 0.9,
                          frozenset({"C":False,"S":True}.items()) : 0.5,
                          frozenset({"C":False,"S":False}.items()): 0.5
                        }.items()),
                   "W": frozenset({
                          frozenset({"S":True, "R":True, "W":True}.items()) : 0.99, 
                          frozenset({"S":True, "R":True, "W":False}.items()): 0.01, 
                          frozenset({"S":True, "R":False,"W":True}.items()) : 0.90, 
                          frozenset({"S":True, "R":False,"W":False}.items()): 0.10, 
                          frozenset({"S":False,"R":True, "W":True}.items()) : 0.90, 
                          frozenset({"S":False,"R":True, "W":False}.items()): 0.10, 
                          frozenset({"S":False,"R":False,"W":True}.items()) : 0.01, 
                          frozenset({"S":False,"R":False,"W":False}.items()): 0.99
                        }.items())
                  }

  print("nodes")
  pprint(nodes)
  print("probabilities")
  pprint(probabilities)

  bayes_net  = BayesNet(nodes, probabilities)
  likelihood = LikelihoodWeighting(bayes_net, {"C":False}, 100000)
  print(likelihood.query({"C":False}))

#start the app
if __name__ == "__main__":
  main()
