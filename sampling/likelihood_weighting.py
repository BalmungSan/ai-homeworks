#imports
from bayes_nets import BayesNet
import random

#
class LikelihoodWeighting:
  #
  def __init__(self, bayes_net, evidence, n):
    self.bayes_net = bayes_net
    self.evidence  = evidence

    #
    self.sample(n)

  def sample(self, n):
    def gen_sample():
      rand = random.random()
      sample = {}
      values = {}

      queue = list(self.bayes_net.net[None])
      while queue:
        node = queue.pop(0)
        name = node.name
        query = {}
        for p in node.parents:
          query[p.name] = sample[p.name]
        if name in self.evidence:
          value        = self.evidence[name]
          query[name]  = value
          sample[name] = value
          values[name] = node.get_probability(query)
        else:
          d_true = query.copy()
          d_true[name] = True
          v_true = node.get_probability(d_true)
          d_false = query.copy()
          d_false[name] = False
          v_false = node.get_probability(d_false)
          if (rand - v_true >= 0):
            #true
            sample[name] = True
            values[name] = v_true
          else:
            #false
            sample[name] = False
            values[name] = v_false

        #adding clidren to queue
        for n in self.bayes_net.net[node]:
          if n not in queue:
            queue.append(n)

      w = 1
      for e in self.evidence:
        w *= values[e]
      return (frozenset(sample.items()), w)

    self.samples = []
    for _ in range(n):
      self.samples.append(gen_sample())

  def query(self, query):
    def total_weight(elems):
      total = 0
      for sample,w in self.samples:
        if frozenset(elems.items()) <= sample:
          total += w
      return total

    return total_weight(query) / total_weight(self.evidence)
