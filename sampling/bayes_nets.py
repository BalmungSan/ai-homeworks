#imports
from typing import Dict, List

#Bayes Node Class
class BayesNode:
  #
  def __init__(self, name: str, probability: Dict[Dict[str,bool], float]):
    self.name        = name
    self.parents     = set()
    self.probability = dict(probability)

  #adding a parent to the node
  def add_parent(self, parent: 'BayesNode'):
    self.parents.add(parent)

  #
  def get_probability(self, keys):
    return self.probability[frozenset(keys.items())]

#Bayes Net Class
class BayesNet:
  #
  def __init__(self, nodes: Dict[str, List[str]],\
               probabilities: Dict[str, Dict[Dict[str,bool], float]]):
    #create nodes
    self.nodes = {}
    for node in nodes:
      self.nodes[node] = BayesNode(node, probabilities[node])

    #create the net
    self.net = {}
    for node_str,children_str in nodes.items():
      node_by     = self.nodes[node_str]
      children_by = set()
      for son_str in children_str:
        children_by.add(self.nodes[son_str])
      self.net[node_by] = children_by

    #add parents to nodes
    for node,children in self.net.items():
      for son in children:
        son.add_parent(node)

    #add the root None node
    nodes_without_parents = set()
    for node_str,node_by in self.nodes.items():
      if not node_by.parents:
        nodes_without_parents.add(node_by)
    self.net[None] = nodes_without_parents
